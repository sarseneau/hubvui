//
//  ViewController.swift
//  MyMercier
//
//  Created by Shawn Arseneau on 10/22/19.
//  Copyright © 2019 Shawn Arseneau. All rights reserved.
//

import UIKit
import MediaPlayer

//DF start
import ApiAI
import AVFoundation
//DF end

class ViewController: UIViewController {

    // bottom label on app for developer feedback on states, etc.
    @IBOutlet var feedbackLabel: UILabel!
    
    // Title only for the speaker toggle
    @IBOutlet var speakerToggleLabel: UILabel!
    
    
    @IBOutlet var testTalk: UIButton!               // text-to-speech of predetermined sentence
    @IBOutlet var speakerToggle: UISwitch!          // toggle to force use of the bottom-speakers on iPhone (or bluetooth headphones).  Required as the default output speaker was the earpiece for the phone and it's quite quiet
    @IBOutlet var tapToTalkButton: UIButton!
    
    var heardLabel: UILabel! // Created at runtime (this would ideally be an output to a predetermined UI element; however, it seems at the moment, this is required
    
    var sub: String!            // subscription key for Microsoft Cognitive Services
    var region: String!         // region associated with the app (defines allowed voices, etc.)
    
    var speechConfig: SPXSpeechConfiguration?
    
    // Initialization function ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Credentials from single user (Shawn Arseneau - Azure)
//        // load Microsoft Cognitive Services (Speech component)
//        sub = "4d13ab5854ef49418d032d57449d5fba"    // azure account: https://azure.microsoft.com/en-us/try/cognitive-services/my-apis/
//        region = "westus"                           // the local region associated with the Microsoft Cog Svc (determines voice, etc.)

        
        sub = "289684a298024d0e9546e3588ce41d90"    // azure account: https://azure.microsoft.com/en-us/try/cognitive-services/my-apis/
        region = "eastus2"                           // the local region associated with
        
        
        // Feedback label (at present, this is REQUIRED as for some reason, we cannot write to labels created in the main storyboard)
        heardLabel = UILabel(frame: CGRect(x: 100, y:400, width: 200, height: 200))
        heardLabel.textColor = UIColor.black
        heardLabel.lineBreakMode = .byWordWrapping
        heardLabel.numberOfLines = 0
        self.view.addSubview(heardLabel)    // adding to the viewController
        
        // Set the UI labels and buttons to the Laura Mercier font type (Gotham)
        feedbackLabel.font = UIFont(name: "Gotham-Thin", size: 14)
        speakerToggleLabel.font = UIFont(name: "Gotham-Bold", size: 14)
        testTalk.titleLabel?.font = UIFont(name: "Gotham-Bold", size: 14)
        tapToTalkButton.titleLabel?.font = UIFont(name: "Gotham-Bold", size: 20)
        heardLabel.font = UIFont(name: "Gotham-Bold", size: 18)
        speakerToggle.setOn(false, animated: false)
        
        //textToSpeech(inputtext: "Welcome!")
    }
    
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    func initializeSpeech() {
        
        do {
            try speechConfig = SPXSpeechConfiguration(subscription: sub, region: region)
        } catch {
            print("speech-to-text error \(error) happened")
            speechConfig = nil
        }
        
        // Speech-to-text settings
        speechConfig?.speechRecognitionLanguage = "en-US"
        
        // Text-to-speech settings
        //speechConfig?.speechSynthesisVoiceName = "en-US-JessaRUS"
        //speechConfig?.speechSynthesisVoiceName = "en-AU-Catherine"
        speechConfig?.speechSynthesisVoiceName = "fr-FR-Julie-Apollo" // has the closest French accent
    }
    
    // Press to initiate speech-to-text +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    @IBAction func tapToTalkPressed(_ sender: UIButton) {
        
        DispatchQueue.global(qos: .userInitiated).async {
            self.speechToText()
        }
        self.feedbackLabel.text = "tap to talk pressed!"
    }
    
    // Test text-to-speech ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    @IBAction func testTalkPressed(_ sender: Any) {
    
        var testSpeech: String!
        
        testSpeech = "Welcome to the Laura Mercier app"
        
        DispatchQueue.global(qos: .userInitiated).async {
            self.textToSpeech(inputtext: testSpeech)
        }
        
        // test to show the audio component is speaking
        self.feedbackLabel.text = "test talk pressed"
    }

    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Speak: send this function a text string and voila!  A synthesized voice speaks the text
    // (text to speech)
    @objc func speak(text: String?) {
        DispatchQueue.global(qos: .userInitiated).async {
            self.textToSpeech(inputtext: text!)
        }
    }
    
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Open the microphone and listen for the complete voice component from the user
    // Upon completion, send to the recognition and output the text (speech-to-text)
    func speechToText() {
        
        initializeSpeech()
        
        let audioConfig = SPXAudioConfiguration()
        
        let reco = try! SPXSpeechRecognizer(speechConfiguration: speechConfig!, audioConfiguration: audioConfig)
        
        reco.addRecognizingEventHandler() {reco, evt in
            print("intermediate recognition result: \(evt.result.text ?? "(no result)")")
            self.updateLabel(text: evt.result.text, color: .gray)
        }
        
        updateLabel(text: "Listening ...", color: .gray)
        print("Listening...")
        
        let result = try! reco.recognizeOnce()
        print("recognition result: \(result.text ?? "(no result)")")
        //speak(text: result.text)
        
        //userColor =
        updateLabel(text: result.text, color: findPrompt(text: result.text))
        
                   //DF START
                 let request = ApiAI.shared().textRequest()
        
                if let text = result.text, text != "" {
                    request?.query = text
                } else {
                    return
                }
        
                request?.setMappedCompletionBlockSuccess({ (request, response) in
                    let response = response as! AIResponse
                    if let textResponse = response.result.fulfillment.speech {
                        self.speak(text: textResponse)
                    }
                }, failure: { (request, error) in
                    print(error!)
                })
        
                ApiAI.shared().enqueue(request)
                
                //DF END
        
    }
    
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Given a string, convert to speech
    func textToSpeech(inputtext: String) {
        
        // early out
        if inputtext.isEmpty {
            print("the text is empty for the speech")
            return
        }
        else {
            print("attempting to say: \(inputtext)")
        }
        
        initializeSpeech()
        
        //
        let audioConfig = SPXAudioConfiguration()
        
        let reco = try! SPXSpeechRecognizer(speechConfiguration: speechConfig!, audioConfiguration: audioConfig)
        
        reco.addRecognizingEventHandler() {reco, evt in
            print("intermediate recognition result: \(evt.result.text ?? "(no result)")")
        }
        
        let result1 = try! reco.recognizeOnce()
        //
        
        let synthesizer = try! SPXSpeechSynthesizer(speechConfig!)

        let result = try! synthesizer.speakText(inputtext)

        if result.reason == SPXResultReason.canceled
        {
            let cancellationDetails = try! SPXSpeechSynthesisCancellationDetails(fromCanceledSynthesisResult: result)
            print("cancelled, detail: \(cancellationDetails.errorDetails!) ")
        } else {
            
 
            
        }
    }
    
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Updating the label (that is created at runtime) to show the interpreted speech-to-text
    func updateLabel(text: String?, color: UIColor) {
        DispatchQueue.main.async {
            self.heardLabel.text = text
            self.heardLabel.textColor = self.findPrompt(text: text)
        }
    }
    
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Quick test for finding keywords within text and visualizing output
    func findPrompt(text: String?) -> UIColor {
        
        if text!.isEmpty {
            return UIColor.gray
        }
                    
        // Look for lipstick
        if text!.contains("red") {
            return UIColor.red
        }
        else if text!.contains("green") {
             return UIColor.green
        }
        else if text!.contains("the rain in spain") {
            return UIColor.yellow
        }
        
        return UIColor.black
    }
    
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // User has opted to toggle audio output
    @IBAction func toggleSpeaker(_ sender: Any) {
        
        if speakerToggle.isOn {
            
            // Forces the audio to output to the speakers (not earpiece)
            do {
                try AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
            } catch let error as NSError {
                print("Audio Session error: \(error.localizedDescription)")
            }
        }
        else {
            do {
                try AVAudioSession.sharedInstance().overrideOutputAudioPort(.none)
            } catch let error as NSError {
                print("Audio Session error: \(error.localizedDescription)")
            }
        }
    }

    
    
}

