//
//  AppDelegate.swift
//  MyMercier
//
//  Created by Shawn Arseneau on 10/22/19.
//  Copyright © 2019 Shawn Arseneau. All rights reserved.
//

import UIKit
import ApiAI //DialogFlow

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // DF start
        let configuration = AIDefaultConfiguration()
        configuration.clientAccessToken = "4275ee9e04fe4031bae9f0aad511f274"
        
        let apiai = ApiAI.shared()
        apiai?.configuration = configuration
        // DF end
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

